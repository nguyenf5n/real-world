import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home'
import Login from './views/Login'
import Register from './views/Register'
import Settings from './views/Settings'
import Profile from './views/Profile'
import CreateArticle from './views/CreateArticle'
import UpdateArticle from './views/UpdateArticle'
import Article from './views/Article'
import Favorite from './views/Favorite'

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      props: true
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings
    },
    {
      path: '/profile/:username',
      name: 'profile',
      component: Profile,
      props: true
    },
    {
      path: '/profile/:username/favorite',
      name: 'favorite',
      component: Favorite,
      props: true
    },
    {
      path: '/editor',
      name: 'CreateArticle',
      component: CreateArticle
    },
    {
      path: '/editor/:slug',
      name: 'UpdateArticle',
      component: UpdateArticle,
      props: true
    },
    {
      path: '/articles/:slug',
      name: 'Article',
      component: Article,
      props: true
    },
    {
      path: '/tag/:tagname',
      name: 'TagArticles',
      component: Home,
      props: true
    },

  ]
})
