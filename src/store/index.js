import Vue from 'vue'
import Vuex from 'vuex'
import articles from './modules/articles'
import tags from './modules/tags'
import auth from './modules/auth'

Vue.use(Vuex)

const state = {

}

const getters = {

}

const mutations = {

}

const actions = {

}

const modules = {
  articles,
  tags,
  auth
}

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules
})
