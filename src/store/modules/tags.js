import ApiService from '@/api';

export default {
  namespaced: true,
  state: {
    tags: []
  },
  // return state
  getters: {},
  // handle state and data
  mutations: {
    getTags(state, data) {
      state.tags = data
    }
  },
  // commit mutations with data
  actions: {
    getTags(context) {
      ApiService.get('tags').then(response => {
        return context.commit('getTags', response.data.tags)
      })
    }
  }
}
